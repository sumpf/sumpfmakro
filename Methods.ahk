;---------Helper-------
breakAllLoops(){
	global perma1
	global perma8
	global permaCyber
	perma1 := 0
	perma8 := 0
	permaCyber := false
}


;---------Desktop------------
;Firefox Tab nach links
tabLeft(){
 Send ^+{Tab}
 return
}
;Firefox Tab nach rechts
tabRight(){
 Send ^{Tab}
 return
}
;Firefox Tab schlie�en
tabClose(){
 Send ^{w}
 return
}

;Fensterrand entfernen
fitWindow(){
 WinSet, Style, -0xC40000, a
 WinMove, a, , 0, 0, % A_ScreenWidth, % A_ScreenHeight
 return
}

;---------------COH----------------------
;Vehicle Push
vehiclePushXButton2(){
 xdir:=1 ;richtungsvektor f�r den push
 ydir:=1
 vlength:=10
 while (GetKeyState("XButton2","P") = 1)
 {
  MouseGetPos,xpos1,ypos1
  Click down right
  MouseMove,%xdir%,%ydir%,0,R
  Sleep, 30
  Click up right
  MouseMove,% - xdir ,% - ydir,0,R  
  MouseGetPos,xpos2,ypos2
  vlength:=Sqrt((xpos2-xpos1)**2+(ypos2-ypos1)**2)
  if (vlength> 15){ ; Richtungsvektor wird neu gesetzt und normiert, falls schnellere mausbewegung als 15px/tick. Es gibt starke Rundungsfehler.
   xdir := (((xpos2-xpos1)*100//vlength))
   ydir := (((ypos2-ypos1)*100//vlength))
  }
 } 
 Click up right
}
return

;R�ckw�rtsfahren
vehicleBackupXbutton1(){
 Click right
 while (GetKeyState("XButton1","P") = 1){
  Sleep, 50
  Send +{Click right}
 }
 Send {Shift up}
}
return

;Reinforce
reinforce(){
 loop 5{
  Send z
  Send z
  Send z
  Send z
  Send z
  Send {Tab}
 }
 return
}

;Countersnipe
countersnipe(){
 Send 1
 Sleep, 20
 Send q
 Sleep, 20
 Send s
 return
}

;Exit Building
exitBuilding(){
 Send {Ctrl Up}
 Send {Tab}
 Send a
 click
return
}
;---------------Coh 2------------------
reinforce2(){
 loop 10{
  Send f
 }
}
return
tacmap2(){
  Send {Numpad0}
}

;---------------Borderlands------------
;Rechtsklickspam
clickspamRightXButton1(){
 Click right
 While (GetKeyState("XButton1","P") = 1){
  Click down right
  ;Sleep, 15
  Click up right
  ;Sleep, 15
 }
}
return
;Linksklickspam
clickspamLeftXButton2(){
SetKeyDelay, 0.5
SetMouseDelay, 0.5
While (GetKeyState("XButton2","P") = 1)
 {
  Send {LButton}
  Sleep, 21
  ;SendInput {LButton up}
  ;Sleep, 5
 }
}
return

;-----------------Diablo 3---------------

;Linksklickspam
clickspam1XButton2(){
While (GetKeyState("XButton2","P") = 1)
 {
  Send 1
  Sleep, 100
 }
}
return

;Linksklickspam
teleportpamXButton1(){
Send 3
While (GetKeyState("XButton1","P") = 1)
 {
  Send 3
  Sleep, 100
 }
}
return

;Itemdrop ; 960, 540
itemdrop(){
	  MouseGetPos,xpos1,ypos1
	  Click down left
	  MouseMove, 960, 540, 0,
	  Click up left
	  MouseMove, xpos1,ypos1, 0,
}
return


togglePerma1(){
	global perma1
	global CFG_COOLDOWN_REDUCTION
	global CFG_COOLDOWN_OFFSET
	d3Cooldown := (6000*(1-CFG_COOLDOWN_REDUCTION) + CFG_COOLDOWN_OFFSET - 150)
	perma1 := 1-perma1
	While (!perma1){
		Send, 1
		Sleep, 50
		Send, 1
		Sleep, 50
		Send, 1
		Sleep, 50
		Sleep,% d3Cooldown/2
		Send, 4
		Sleep,% d3Cooldown/2
	}
	return
}
return

--- Cyberpunk ---
loopCyberCraft(){
	global permaCyber
	global CFG_CYBER_HOLD_TIME
	While (permaCyber){
		Send, {LButton Down}
		Sleep, % CFG_CYBER_HOLD_TIME
		Send, {LButton Up}
		Sleep, 20
	}
}
return

toggleCyberCraft(){
	global permaCyber
	permaCyber := !permaCyber
	loopCyberCraft()
}
return
;------------------ Guild Wars -------------------
perma8loop(){
	global perma8
	global CFG_COOLDOWN_GW
	while (perma8){
		Send, 8
		Sleep, 50
		Send, 8
		Sleep, 50
		Send, 8
		Sleep, 50
		Send, 8
		Sleep, % CFG_COOLDOWN_GW
	}
	return
}

togglePerma8(){
	global perma8
	perma8 := 1-perma8
	perma8loop()
}