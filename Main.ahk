; AutoHotkey Skript by Jonas Marasus.
 
; Fenster Button, 	Strg+Alt+Shift+8	!^+8
; + Button, 		Strg+Alt+Shift+9  	!^+9
; - Button, 		Strg+Alt+Shift+0  	!^+0

;----------------------------INITIALISIERUNG------------------
#MaxThreadsPerHotkey 2
;Mode 1=Desktop, 2=Borderlands 2 3=Company of Heroes 4= Coh 2 5= d3 6=Fallout 4 7=guild Wars
Mode = 1 
Reinforcing = 0
Exiting = 0
CFG_COOLDOWN_OFFSET := 200	; Adjust to account for lag/interface delay
CFG_COOLDOWN_REDUCTION := (0.52)
CFG_COOLDOWN_GW := 10100
CFG_CYBER_HOLD_TIME := 20
perma1 := 0
perma8 := 0
permaCyber := false


;---------------------------------GUI-------------------------
Gui, Add, Text,w200 vGuiModus, Desktop aktiv
Gui, Add, Button,, Desktop
Gui, Add, Button,, Borderlands 2
Gui, Add, Button,, Company of Heroes
Gui, Add, Button,, Company of Heroes 2
Gui, Add, Button,, Diablo 3
Gui, Add, Button,, Fallout 4
Gui, Add, Button,, Guild Wars
Gui, Add, Text, w190, Sumpfmakro version 3.0
#include Methods.ahk
return

!^+0::
Gui, Show,w200 h 200 , Sumpfmakro 3.0
return

;-------------------------------SET-MODE----------------------

ButtonDesktop:
$!^+1::
Mode = 1
GuiControl,, GuiModus, Desktop aktiv.
return

ButtonBorderlands2:
$!^+2::
Mode = 2
GuiControl,, GuiModus, Borderlands 2 aktiv.
return

ButtonCompanyofHeroes:
$!^+3::
Mode = 3
GuiControl,, GuiModus, Company of Heroes aktiv.
return

ButtonCompanyofHeroes2:
$!^+4::
Mode = 4
GuiControl,, GuiModus, Company of Heroes 2 aktiv.
return

ButtonDiablo3:
$!^+5::
Mode = 5
GuiControl,, GuiModus, Diablo 3 aktiv.
return

ButtonFallout4:
$!^+6::
Mode = 6
GuiControl,, GuiModus, Fallout 4 aktiv.
return

ButtonGuildwars:
$!^+7::
Mode = 7
GuiControl,, GuiModus, Guild Wars aktiv.
return

;-------------------------------Belegung-----------------------
Pause::Pause
return
; Vortaste:
XButton2::
if (mode = 1){
 tabRight()					; Tab nach rechts
} else if (mode=3){
 vehiclePushXButton2()		; Automatisierter Push
} else if (mode=2){
 clickspamLeftXButton2()	; Klickspam
} else if (mode=5){
 clickspam1XButton2()
} else if (mode=6){
 send {Enter}
} else if (mode=7){
 send {Numpad1}
} else tabRight()

return

; R�cktaste:
XButton1::
if (mode=1){
 tabLeft()					; Tab nach links
}else if (mode=3){
 vehicleBackupXButton1()	; R�ckw�rtsfaheren
}else if (mode=2){
 clickspamRightXButton1()	; Rechtsklickspam
}else if (mode=5){
 teleportpamXButton1()		; Teleport
}else if (mode=6){
 send {Tab}
}else if (mode=7){
 send {Numpad2}
}
return

; Fenstertaste:
!^+8::
 if (mode=1){
 tabClose()					; Tab schlie�en
 } else if (mode = 5){
 send +{Click left}		; shift + click
 }
 
return
 
; Strg + y
;^y::
; if (mode=4){
;  reinforce2()
; } else if(mode=3){
;  if(Reinforcing = 0){
;   Reinforcing = 1
;   reinforce()  				; Reinforce
;   Reinforcing = 0
;  }
; } else if(mode=4){
;	reinforce2()				; Reinforce
; } else if(mode=5){;
;	perma1 = 0
; }
; else
;return

; Strg + Alt + Shift + f
!^+f::
 if(mode=3||mode=4){
  fitWindow()
 } else if (mode=7){
   togglePerma8()
 } else if (mode=2){
    toggleCyberCraft()
 }
return

!^+d::
 breakAllLoops()
return

; Strg + d
;^d::
;	MsgBox,% perma1
;	if(mode=4){
;		tacmap2()				;taktische Karte
;	}
;	if(mode=5){
;		togglePerma1()
;	}

;return